# configs

A place for misc shell scripts and dotfiles.


## Linux software

| Name | description | install method |
| ------ | ------ | ------ |
| cell | cell | cell |
| cell | cell | cell |
| cell | cell | cell |

## Installation methods

* snap
* apt
* flatpak
* tar
* deb


## apps to add to chart
* Blender
* Docker
* Firefox
* Syncthing
* Keybase
* Standard Notes
* Thunderbird
* Mattermost
* Mullvad
* qBittorrent
* VirtualBox
* Vim
* VSCodium
* VLC
* Zoom
