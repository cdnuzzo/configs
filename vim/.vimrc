set number            	" line numbering
set mouse=a            	" mouse support
set background=dark    	" dark mode syntax highlighting
" (use light for lighter backgrounds)

set backspace=2        	" backspace fix on some computers (like macs)
colorscheme desert    	" set syntax highlighting color scheme
syntax on            	" enable syntax highlighting
set autoindent        	" enable auto indentation

set shiftwidth=4        " use 4 spaces for tabs
set softtabstop=4
set expandtab