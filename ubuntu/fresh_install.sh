#!/bin/bash
# ONLY for Ubuntu 18.04 based distros (not sure if anyother will break or not)

# needs some work but the idea is to run altogether or can take portions to install programs as need be
# program installations are preferably from source, although some are from aptitude since they are up to date enough but might change that as I see fit


sudo apt update -y
sudo apt upgrade -y

# pip
sudo apt install python3-venv python3-pip -y

#aws cli
pip3 install awscli

# keybase
curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb
sudo dpkg -i keybase_amd64.deb
sudo apt-get install -f -y

# htop 
sudo apt install htop -y

# docker
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" -y
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose -y

# zappa 
sudo docker pull lambci/lambda:build-python3.6

# vim 
touch ~/.vimrc

# virtualbox
# needs the following in /etc/apt/sources.list -> deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian <mydist> contrib
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -


# Standard Notes
wget https://github.com/standardnotes/desktop/releases/download/v3.0.6/standard-notes-3.0.6-x86_64.AppImage
mv standard-notes-3.0.6-x86_64.AppImage ~/standard-notes-3.0.6-x86_64.AppImage
cd ~
chmod a+x standard-notes-3.0.6-x86_64.AppImage 
./standard-notes-3.0.6-x86_64.AppImage

# install snapd
sudo apt install snapd -y

# install Slack
sudo snap install slack --classic

# zoom
wget https://zoom.us/client/latest/zoom_amd64.deb
sudo gdebi zoom_amd64.deb -y 


# mullvad vpn
wget https://mullvad.net/media/app/MullvadVPN-2019.3_amd64.deb  
sudo apt -y install gdebi-core
sudo gdebi MullvadVPN-2019.3_amd64.deb


# set up syncthing with docker
sudo  docker run -d --network=host -v $HOME:/var/syncthing syncthing/syncthing:latest

# tree
sudo apt install tree -y

# gotta have some fun. 
sudo apt install cmatrix -y

# nextcloud desktop client
sudo add-apt-repository ppa:nextcloud-devs/client -y
sudo apt install nextcloud-client -y

# terraform
wget https://releases.hashicorp.com/terraform/0.12.23/terraform_0.12.23_linux_amd64.zip
sudo unzip ./terraform_0.11.13_linux_amd64.zip -d /usr/local/bin/

# ansible

sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible

